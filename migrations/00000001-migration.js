'use strict';

var Sequelize = require('sequelize');

/**
 * Actions summary:
 *
 * createTable "UserModels", deps: []
 *
 **/

var info = {
  revision: 1,
  name: 'migration',
  created: '2023-06-15T14:31:36.452Z',
  comment: '',
};

var migrationCommands = [
  {
    fn: 'createTable',
    params: [
      'SequelizeMetaMigrations',
      {
        revision: {
          primaryKey: true,
          type: Sequelize.UUID,
        },
        name: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        state: {
          allowNull: false,
          type: Sequelize.JSON,
        },
      },
      {},
    ],
  },
  {
    fn: 'bulkDelete',
    params: [
      'SequelizeMetaMigrations',
      [
        {
          revision: info.revision,
        },
      ],
      {},
    ],
  },
  {
    fn: 'bulkInsert',
    params: [
      'SequelizeMetaMigrations',
      [
        {
          revision: info.revision,
          name: info.name,
          state:
            '{"revision":1,"tables":{"UserModels":{"tableName":"UserModels","schema":{"id":{"seqType":"Sequelize.INTEGER","primaryKey":true,"autoIncrement":true},"createdAt":{"seqType":"Sequelize.DATE"},"updatedAt":{"seqType":"Sequelize.DATE"},"deletedAt":{"seqType":"Sequelize.DATE"},"username":{"seqType":"Sequelize.STRING","allowNull":false},"password":{"seqType":"Sequelize.STRING","allowNull":false},"email":{"seqType":"Sequelize.STRING","allowNull":false,"unique":true}},"indexes":{}}}}',
        },
      ],
      {},
    ],
  },

  {
    fn: 'createTable',
    params: [
      'UserModels',
      {
        id: {
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        createdAt: {
          type: Sequelize.DATE,
        },
        updatedAt: {
          type: Sequelize.DATE,
        },
        deletedAt: {
          type: Sequelize.DATE,
        },
        username: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        password: {
          allowNull: false,
          type: Sequelize.STRING,
        },
        email: {
          unique: true,
          allowNull: false,
          type: Sequelize.STRING,
        },
      },
      {},
    ],
  },
];

var rollbackCommands = [
  {
    fn: 'bulkDelete',
    params: [
      'SequelizeMetaMigrations',
      [
        {
          revision: info.revision,
        },
      ],
      {},
    ],
  },

  {
    fn: 'dropTable',
    params: ['UserModels'],
  },
];

module.exports = {
  pos: 0,
  up: function (queryInterface, Sequelize) {
    var index = this.pos;
    return new Promise(function (resolve, reject) {
      function next() {
        if (index < migrationCommands.length) {
          let command = migrationCommands[index];
          console.log('[#' + index + '] execute: ' + command.fn);
          index++;
          queryInterface[command.fn]
            .apply(queryInterface, command.params)
            .then(next, reject);
        } else resolve();
      }
      next();
    });
  },
  down: function (queryInterface, Sequelize) {
    var index = this.pos;
    return new Promise(function (resolve, reject) {
      function next() {
        if (index < rollbackCommands.length) {
          let command = rollbackCommands[index];
          console.log('[#' + index + '] execute: ' + command.fn);
          index++;
          queryInterface[command.fn]
            .apply(queryInterface, command.params)
            .then(next, reject);
        } else resolve();
      }
      next();
    });
  },
  info: info,
};
