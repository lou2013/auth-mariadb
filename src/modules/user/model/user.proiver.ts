import { AppRepository } from 'src/common/constants/app.repository';
import { UserModel } from './user.model';

export const UserProviders = [
  {
    provide: AppRepository.User,
    useValue: UserModel,
  },
];
