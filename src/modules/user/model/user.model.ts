import { Column, DataType, Table } from 'sequelize-typescript';
import { BaseModel } from 'src/common/model/base.model';
import { UserInterface } from '../interface/user.interface';

@Table({ paranoid: true })
export class UserModel extends BaseModel implements UserInterface {
  @Column({ type: DataType.STRING, allowNull: false })
  username: string;

  @Column({ type: DataType.STRING, allowNull: false })
  password: string;

  @Column({ type: DataType.STRING, allowNull: false, unique: true })
  email: string;
}
