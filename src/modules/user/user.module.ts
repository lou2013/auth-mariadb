import { forwardRef, Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth-module';
import { UserController } from './controller/user.controller';
import { UserFactory } from './entity/user-factory.helper';
import { UserProviders } from './model/user.proiver';
import { UserService } from './service/user.service';

@Module({
  imports: [forwardRef(() => AuthModule)],
  controllers: [UserController],
  providers: [...UserProviders, UserService, UserFactory],
  exports: [UserService],
})
export class UserModule {}
