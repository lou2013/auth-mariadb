import { Expose } from 'class-transformer';
import {
  IsEmail,
  IsString,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { BaseDto } from 'src/common/dto/base.dto';
import { UserInterface } from '../interface/user.interface';

export class UserDto extends BaseDto implements UserInterface {
  @IsString()
  @MinLength(5)
  @Expose()
  username: string;

  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  @Expose({ toClassOnly: true })
  @MinLength(8)
  @MaxLength(20)
  @IsString()
  password: string;

  @IsString()
  @IsEmail()
  @Expose()
  email: string;

  constructor(data: Partial<UserDto>) {
    super(data);
    Object.assign(this, data);
  }
}
