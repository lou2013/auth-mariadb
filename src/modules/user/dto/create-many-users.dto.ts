import { Expose, Type } from 'class-transformer';
import { IsArray, IsNotEmpty, ValidateNested } from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class CreateManyUsersDto {
  @Type(() => CreateUserDto)
  @IsArray()
  @IsNotEmpty()
  @ValidateNested()
  @Expose()
  users: CreateUserDto[];
}
