import { Inject, Injectable } from '@nestjs/common';
import { AppRepository } from 'src/common/constants/app.repository';
import { CreateUserDto } from '../dto/create-user.dto';
import { UserFactory } from '../entity/user-factory.helper';
import { User } from '../entity/user.entity';
import { UserServiceInterface } from '../interface/user-service.interface';
import { UserModel } from '../model/user.model';
import { MakeNullishOptional } from 'sequelize/types/utils';
import { Op } from 'sequelize';
import { hashData } from '../helpers/hash-password.helper';
@Injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @Inject(AppRepository.User) private userRepository: typeof UserModel,
    private readonly userFactory: UserFactory,
  ) {}

  async createMany(data: CreateUserDto[]): Promise<User[]> {
    const passwordsPromises = data.map((d) => hashData(d.password));
    const passwords = await Promise.all(passwordsPromises);
    data.map((v, i) => (v.password = passwords[i]));
    const result = await this.userRepository.bulkCreate(
      data as MakeNullishOptional<User>[],
    );
    return this.userFactory.createMany(result.map((d) => d.toJSON()));
  }

  async create(data: CreateUserDto): Promise<User> {
    data.password = await hashData(data.password);
    const result = await this.userRepository.create(
      data as MakeNullishOptional<User>,
    );
    return this.userFactory.create(result?.toJSON());
  }

  async findById(id: number): Promise<User> {
    const result = await this.userRepository.findByPk(id);
    return this.userFactory.create(result?.toJSON());
  }

  async findByEmail(email: string): Promise<User> {
    const result = await this.userRepository.findOne({
      where: { email: { [Op.eq]: email } },
    });

    return this.userFactory.create(result?.toJSON());
  }
}
