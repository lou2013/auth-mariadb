import { BaseEntity } from 'src/common/entity/base.entity';
import { UserInterface } from '../interface/user.interface';

export class User extends BaseEntity implements UserInterface {
  username: string;

  password: string;

  email: string;

  constructor(data: Partial<User>) {
    super(data);
    Object.assign(this, data);
  }
}
