import { Expose } from 'class-transformer';
import {
  IsString,
  MinLength,
  Matches,
  MaxLength,
  IsEmail,
} from 'class-validator';
import { BaseDto } from 'src/common/dto/base.dto';

export class SignupDto extends BaseDto {
  @IsString()
  @MinLength(5)
  @Expose()
  username: string;

  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, {
    message: 'password too weak',
  })
  @Expose()
  @MinLength(8)
  @MaxLength(20)
  @IsString()
  password: string;

  @IsString()
  @IsEmail()
  @Expose()
  email: string;

  constructor(data: Partial<SignupDto>) {
    super(data);
    Object.assign(this, data);
  }
}
