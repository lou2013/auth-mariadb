import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { AuthModule } from './auth/auth-module';
import { UserModule } from './user/user.module';
import { V1Routes } from './v1.routes';

@Module({
  imports: [RouterModule.register(V1Routes), UserModule, AuthModule],
})
export class V1Module {}
