export class AppConfigs {
  public static APP = 'app';

  public static MAIN_DATABASE = 'sequelize_maria';

  public static REDIS = 'redis';
}
