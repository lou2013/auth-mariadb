import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '@nestjs/config';
import { Dialect } from 'sequelize/types';
import { AppConfigs } from 'src/common/constants/app.configs';
import { SequelizeConfig } from 'src/common/configs/sequelize.config.interface';
import { UserModel } from 'src/modules/user/model/user.model';
import { AppConfig } from 'src/common/configs/app-config.interface';
import { SequelizeTypescriptMigration } from 'sequelize-typescript-migration-v2';
import { join } from 'path';
export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async function (
      configService: ConfigService,
    ): Promise<Sequelize[]> {
      const databaseConfig = configService.get<SequelizeConfig>(
        AppConfigs.MAIN_DATABASE,
      );
      const sequelize = [
        new Sequelize({
          sync: { force: true },
          dialect: databaseConfig.dialect as Dialect,
          database: databaseConfig.database,
          username: databaseConfig.username,
          password: databaseConfig.password,
          host: databaseConfig.host,
          port: databaseConfig.port,
          ssl: databaseConfig.ssl,
          timezone: databaseConfig.timezone,
          repositoryMode: false,
          protocol: 'tcp',
          models: [UserModel],
        }),
      ];
      try {
        if (
          configService.get<AppConfig>(AppConfigs.APP).environment ===
          'development'
        ) {
          await SequelizeTypescriptMigration.makeMigration(
            sequelize[0] as any,
            {
              outDir: join(process.cwd(), 'migrations'),
              migrationName: 'migration',
            },
          );
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      }
      return sequelize;
    },
    inject: [ConfigService],
  },
];
