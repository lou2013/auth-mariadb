import { Global, Module } from '@nestjs/common';
import { SequelizeModule } from './sequelize/sequelize.module';

@Global()
@Module({
  imports: [SequelizeModule],
  exports: [SequelizeModule],
})
export class DatabaseModule {}
