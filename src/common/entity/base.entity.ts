export class BaseEntity {
  id: number;

  createdAt: Date;

  updatedAt: Date;

  deletedAt: Date;

  constructor(data: Partial<BaseEntity>) {
    Object.assign(this, data);
  }
}
