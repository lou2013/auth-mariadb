import {
  ArgumentsHost,
  ExceptionFilter,
  HttpStatus,
  UnprocessableEntityException,
} from '@nestjs/common';
import { Response } from 'express';
import { UniqueConstraintError } from 'sequelize';
export class ErrorFilter implements ExceptionFilter {
  catch(exception: any, host: ArgumentsHost): void {
    const response = host.switchToHttp().getResponse<Response>();
    let message: string = exception.message;
    let status: number = exception.status;
    let errorFields: unknown;
    if (exception instanceof UniqueConstraintError) {
      status = HttpStatus.CONFLICT;
      message = exception.message;
      errorFields = exception.errors.map((error) => ({
        message: error.message,
        path: error.path,
        value: error.value,
      }));
    }
    if (exception instanceof UnprocessableEntityException) {
      response.status(exception.getStatus()).send(exception.getResponse());
      return;
    } else if (!message && !status) {
      message = 'Internal Server Error';
      status = 500;
    }
    response.status(status).send({
      message: message,
      errorFields,
    });
  }
}
